# Datadog-Dashboard

## Summary
This is the module that creates Datadog dashboard.

## Usage
Since version 2023.09.15 the module has been updated to use implicit provider passing.
This requires the provider to be declared in the root module' like this:
```
provider "datadog" {
  api_url = <DATADOG_API_URL>
  api_key = <DATADOG_API_KEY>
  app_key = <DATADOG_APP_URL>
}
```
For the instructions how to set values `DATADOG_API_URL`, `DATADOG_API_KEY`
and `DATADOG_APP_URL` in GEL, please check this document:
[this document](https://cnfl.extge.co.uk/display/GOLD/Datadog%3A+Dashboard+Creation+and+Management).
```
module "datadog-dashboard" {
  source    = <path to this repository, inculding desired tag>
  json_file = file("${path.module}/config-files/datadog_dashboard.json")
  region    = var.region
}
```

`json_file` contains the path to Datadog dashboard json file

Example of `datadog_dashboard.json` file:

```
{
  "title": "Service Management VDIs",
  "description": "Health of the Service Management WorkSpaces",
  "widgets": [
    {
      "definition": {
        "title": "Total WorkSpaces",
        "type": "timeseries",
        "requests": [
          {
            "q": "sum:aws.workspaces.available{account_id:193330866083}"
          }
        ]
      }
    },
    {
      "definition": {
        "title": "WorkSpaces automation lambda errors",
        "type": "timeseries",
        "requests": [
          {
            "q": "avg:aws.lambda.errors{account_id:193330866083,functionname:workspacesautomation}"
          }
        ]
      }
    }
  ],
  "layout_type": "ordered",
  "is_read_only": true,
  "reflow_type": "auto"
```

For more details please check [Terraform registry](https://registry.terraform.io/providers/DataDog/datadog/latest/docs/resources/dashboard_json)

Prerequisites:
AWS account is [integrated with Datadog](https://gitlab.com/genomicsengland/opensource/terraform-modules/datadog-integration)

Limitations:
This module is intended to be used within your GitLab CI/CD pipeline, using
GEL standardized [GitLab runners](https://gitlab.com/genomicsengland/cloud/aws-core/shared/gitlab-runner)

## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_datadog"></a> [datadog](#provider\_datadog) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [datadog_dashboard_json.dashboard_json](https://registry.terraform.io/providers/hashicorp/datadog/latest/docs/resources/dashboard_json) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_enabled"></a> [enabled](#input\_enabled) | Toggles creation of resources and data sources in this repository | `bool` | `true` | no |
| <a name="input_json_file"></a> [json\_file](#input\_json\_file) | Path to the datadog dashboard json file | `string` | n/a | yes |
| <a name="input_region"></a> [region](#input\_region) | AWS region | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_id"></a> [id](#output\_id) | The ID of this dashboard. |
| <a name="output_url"></a> [url](#output\_url) | The URL of the dashboard |
